chrome.runtime.onInstalled.addListener(function () {
    function updateRequestArray(name, details) {
        chrome.storage.local.get(name, function (data) {
            console.log(data[name]);
            let requests = data && data[name] ? data[name] : [];

            requests.unshift(details);

            chrome.storage.local.set({[name]: requests}, function () {});
        });
    }

    function blockRequest(request) {
        updateRequestArray('blockedRequests', request);
        return {cancel: true};
    }

    function updateFilters(urls) {
        if (chrome.webRequest.onBeforeRequest.hasListener(blockRequest)) {
            chrome.webRequest.onBeforeRequest.removeListener(blockRequest);
        }
        chrome.webRequest.onBeforeRequest.addListener(blockRequest, {urls}, ['blocking']);
    }

    fetch('http://192.168.30.14:8080/api/rules')
        .then(result => result.json())
        .then(rules => {
            const regexArray = rules
                .filter(rule => rule.regexPattern)
                .map(rule => rule.regexPattern);
            updateFilters(regexArray);
        });

    chrome.webRequest.onBeforeRequest.addListener((request) => {
        updateRequestArray('allRequests', request);
    }, {
        urls: [
            "<all_urls>"
        ]
    })
});