const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');
// const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: {
        background: './src/background.js',
        options: './src/options.js',
        popup: './src/popup.js',
        'content-script': './src/content-script.js',
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/dist'
    },
    plugins: [
        new CopyPlugin([
            { from: 'src/**/*.html', flatten: true},
           'src/manifest.json',
        ]),
        //new CleanWebpackPlugin(),
        new WriteFilePlugin()
    ],
};